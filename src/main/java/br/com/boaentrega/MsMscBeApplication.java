package br.com.boaentrega;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsMscBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsMscBeApplication.class, args);
	}

}
